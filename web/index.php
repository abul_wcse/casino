<?php
require_once __DIR__ . '/../vendor/autoload.php';

$router = new \Skybet\Managers\Router\RouterManager();
$router->dispatch();