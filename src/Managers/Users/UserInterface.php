<?php
/**
 * Created by PhpStorm.
 * User: abulh
 * Date: 22/06/2017
 * Time: 10:37
 */

namespace Skybet\Managers\Users;


interface UserInterface
{
    /**
     * @return []
     */
    public function getUserDetails();

    /**
     * @param $betValue
     * @return bool
     */
    public function canUserBet($betValue);
}