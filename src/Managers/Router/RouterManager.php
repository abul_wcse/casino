<?php

namespace Skybet\Managers\Router;

class RouterManager
{
    /**
     * @var string
     */
    private $routerConfig;
    /**
     * @var \Klein\Klein
     */
    private $klein;

    public function __construct()
    {
        $this->klein = new \Klein\Klein();
        $this->routerConfig = __DIR__ . '/../../../config/routes.yml';
        $this->loadAllRoutes();
    }

    /**
     * Load all routes
     */
    public function loadAllRoutes()
    {
        $routes = yaml_parse_file($this->routerConfig);
        foreach ($routes['routes'] as $route) {
            list($className, $method) = explode("::", $route[2]);
            $this->klein->respond($route[0], $route[1],[new $className(), $method]);
        }
    }

    /**
     * @return \Klein\DataCollection\RouteCollection
     */
    public function getAllRoutes()
    {
        return $this->klein->routes();
    }

    public function dispatch()
    {
        $this->klein->dispatch();
    }
}