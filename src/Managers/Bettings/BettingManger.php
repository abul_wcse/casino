<?php

namespace Skybet\Managers\Bettings;

use Skybet\Exceptions\BettingTypeAdaptorNotFoundException;
use Skybet\Managers\Bettings\Adpaters\ColorBet;
use Skybet\Managers\Bettings\Adpaters\SingleBet;
use Skybet\Models\Bet;

class BettingManger
{
    /**
     * @var BettingInterface[]
     */
    protected $adaptors;

    public function __construct()
    {
        $this->adaptors = [
            new SingleBet(),
            new ColorBet()
        ];
    }

    /**
     * Get the amount won or lost for the betting in a specific betting type
     *
     * @param Bet[] $bets
     * @param int $winningField
     *
     * @return array
     */
    public function getBettingResultForBettingType($bets, $winningField)
    {
        $response = [];
        foreach ($bets as $bet) {
            $adaptor = $this->getBettingAdaptor($bet->getType());
            $response[$bet->getOption()] = $adaptor->calculateFinalAmount($bet->getOption(), $winningField, $bet->getValue());
        }
        return $response;
    }

    /**
     * @param $bettingType
     * @return mixed|BettingInterface
     * @throws BettingTypeAdaptorNotFoundException
     */
    private function getBettingAdaptor($bettingType)
    {

        foreach ($this->adaptors as $adaptor) {
            if ($adaptor->name() === $bettingType && class_implements($adaptor) != 'Skybet\Managers\Bettings\BettingInterface') {
                return $adaptor;
            }
        }
        throw new BettingTypeAdaptorNotFoundException("Unable to find adptor for " . $bettingType);
    }

    /**
     * @return BettingInterface[]
     */
    public function getAdaptors()
    {
        return $this->adaptors;
    }

}