<?php

namespace Skybet\Managers\Bettings\Adpaters;

use Skybet\Managers\Bettings\BettingInterface;

class SingleBet implements BettingInterface
{
    /**
     * @return string
     */
    public function name()
    {
        return 'Single Bet';
    }

    /**
     * Check whether the given betting has won
     *
     * @param int $bettedField
     * @param int $winningField
     *
     * @return bool
     */
    public function hasOwn($bettedField, $winningField)
    {
        if (intval($bettedField) === intval($winningField)) {
            return true;
        }
        return false;
    }

    /**
     * Calculate the amount the betting resulted
     *
     * @param int $bettedField
     * @param int $winningField
     * @param float $bettingAmount
     *
     * @return float
     */
    public function calculateFinalAmount($bettedField, $winningField, $bettingAmount)
    {
        if ($this->hasOwn($bettedField, $winningField)) {
            return $bettingAmount * 3;
        }
        return 0;
    }
}