<?php

namespace Skybet\Managers\Bettings\Adpaters;

use Skybet\Managers\Bettings\BettingInterface;

class ColorBet implements BettingInterface
{
    /**
     * Get the betting type name
     *
     * @return string
     */
    public function name()
    {
        return 'Color Bet';
    }

    /**
     * Calculate the amount the betting resulted
     *
     * @param int $bettedField
     * @param int $winningField
     * @param float $bettingAmount
     *
     * @return float
     */
    public function calculateFinalAmount($bettedField, $winningField, $bettingAmount)
    {
        // TODO: Implement calculateFinalAmount() method.
    }
}