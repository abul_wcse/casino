<?php

namespace Skybet\Managers\Bettings;

interface BettingInterface
{

    /**
     * Get the betting type name
     *
     * @return string
     */
    public function name();

    /**
     * Calculate the amount the betting resulted
     *
     * @param int $bettedField
     * @param int $winningField
     * @param float $bettingAmount
     *
     * @return float
     */
    public function calculateFinalAmount($bettedField, $winningField, $bettingAmount);
}