<?php

namespace Skybet\Managers\Bettings;


class RouletteWheel
{
    /**
     * @return int
     */
    public static function getResult()
    {
        return rand(1,36);
    }
}