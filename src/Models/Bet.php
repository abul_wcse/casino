<?php

namespace Skybet\Models;


class Bet
{
    /**
     * Store the type of the bet
     *
     * @var string
     */
    protected $type;

    /**
     * Contains the chosen criteria of the bet
     *
     * @var string
     */
    protected $option;

    /**
     * The value of the bet
     *
     * @var float
     */
    protected $value;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Bet
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param string $option
     * @return Bet
     */
    public function setOption($option)
    {
        $this->option = $option;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return Bet
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}