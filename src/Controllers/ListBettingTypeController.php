<?php

namespace Skybet\Controllers;


use Klein\Request;
use Klein\Response;
use Skybet\Managers\Bettings\BettingManger;

class ListBettingTypeController extends AbstractControllers
{

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function get(Request $request, Response $response)
    {
        $betManger = new BettingManger();
        $types = array_map(create_function('$item', 'return $item->name();'), $betManger->getAdaptors());
        return $response->json($types);
    }
}