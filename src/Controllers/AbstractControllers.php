<?php

namespace Skybet\Controllers;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Skybet\Managers\Users\UserInterface;

abstract class AbstractControllers
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var UserInterface
     */
    protected $user;
    /**
     * AbstractControllers constructor.
     */
    public function __construct()
    {
        $this->logger = new Logger('application');
        $this->logger->pushHandler(new StreamHandler(__DIR__ .'/../../logs/application.log', Logger::WARNING));
        //Set session and other details that can be used across all the controller
    }
}