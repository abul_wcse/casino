<?php


namespace Skybet\Controllers;


use Exception;
use Klein\Request;
use Klein\Response;
use Skybet\Exceptions\HttpArgumentNotFoundException;
use Skybet\Exceptions\HttpInvalidParameterException;
use Skybet\Managers\Bettings\BettingManger;
use Skybet\Managers\Bettings\RouletteWheel;
use Skybet\Models\Bet;

class ResultController extends AbstractControllers
{
    /**
     * Get Result of the Betting
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function get(Request $request, Response $response)
    {
        try {
            $betType = $request->param('betType');
            $bettingInput = $request->param('betting');
            if (empty($betType)) {
                throw new HttpArgumentNotFoundException("betType argument missing");
            }
            if (empty($bettingInput)) {
                throw new HttpArgumentNotFoundException("betting argument missing");
            }
            $bettingManager = new BettingManger();
            $bets = [];
            foreach ($bettingInput as $betting) {
                list($field, $amount) = explode(',', $betting);
                if (intval($field) > 36 || intval($field) < 0) {
                    throw new HttpInvalidParameterException("Invalid betting field");
                }
                $bet = new Bet();
                $bet->setValue($amount)
                    ->setType($betType)
                    ->setOption($field);
                $bets[] = $bet;
            }
            $winningField = RouletteWheel::getResult();
            $result['roulette_wheel_result'] = $winningField;
            $result['bet_result'] = $bettingManager->getBettingResultForBettingType($bets, $winningField);
            return $response->json($result);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return $response->json(['_error' => $e->getMessage()]);
        }
    }
}