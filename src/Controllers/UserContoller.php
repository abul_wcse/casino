<?php

namespace Skybet\Controllers;


use Klein\Request;
use Klein\Response;

class UserContoller extends AbstractControllers
{
    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getUserDetails(Request $request, Response $response)
    {
        return $response->json($this->user->getUserDetails());
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function canUserBet(Request $request, Response $response)
    {
        $betAmount = $request->param('betAmount');
        return $response->json(['betAmount' => $betAmount, 'canBet' => $this->user->canUserBet($betAmount)]);
    }
}