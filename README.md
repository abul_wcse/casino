# Skybet Roulette Wheel casino

## Installation

### System Requirement
* PHP 5.6 or above
* Apache 2.4
* Pecl-Yaml

### Steps to install in a cent OS system

1. Clone the repository to /var/www/.
2. Install composer 
3. Enter into the project directory and
do a composer install.
4. Create a Apache Virtual host config. The document root should be the path of the web folder in the project.
5. Enable Klein routing by adding the folling into the apache vHost config.

           <Directory /var/www/casino/web>
                   RewriteEngine On
                   RewriteCond %{REQUEST_FILENAME} !-f
                   RewriteRule . /index.php [L]
                   AllowOverride None
                   Order allow,deny
                   allow from all
           </Directory>

6. Restart Apache

### API Endpoint


#### /result

Arguments

|Field|Type|Required|Description|
|-----|----|--------|-----------|
|betting| array| yes| The field user selected and the bet amount the user specified separated by comma|
|betType| string|yes| The type of Roulette bet made by the user|
 
 
 ##### Example
 
 `/result?betting[]=1,20&betting[]=12,20&betting[]=10,10&betType=Single%20Bet`
 
 #### Response
 The response is of JSON format. The response will contain 2 main fields
 1. roulette_wheel_result, The number in which the Roulette wheel has stopped.
 2. bet_result, the array of the field user selected and the amount he won
 
 ##### Example
 
 ``` 
 {
    "roulette_wheel_result":33,
    "bet_result":{
        "1":0,
        "12":0,
        "10":0
    }
 }
 ```
 
 ### How to add/extend this system to support different bet type?
 
 The system is designed to have a minimal effort of enabling or adding a new bet type.
 To add a new bet type, write a adptor that implements `Skybet\Managers\Bettings\BettingInterface` and register the new bet type into `Skybet\Managers\Bettings\BettingManger` class.  