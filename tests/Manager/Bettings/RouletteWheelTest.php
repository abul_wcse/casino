<?php

namespace Skybet\Tests\Managers\Bettings;


use PHPUnit_Framework_TestCase;
use Skybet\Managers\Bettings\RouletteWheel;

class RouletteWheelTest extends PHPUnit_Framework_TestCase
{

    public function testGetResult()
    {
        $winningField = RouletteWheel::getResult();
        $this->assertTrue(($winningField > 0 && $winningField <= 36));
    }
}