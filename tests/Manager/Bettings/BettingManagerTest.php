<?php

namespace Skybet\Tests\Managers\Bettings;


use PHPUnit_Framework_TestCase;
use Skybet\Exceptions\BettingTypeAdaptorNotFoundException;
use Skybet\Managers\Bettings\BettingManger;
use Skybet\Models\Bet;

class BettingManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var BettingManger
     */
    public $bettingManager;

    /**
     * @var Bet
     */
    public $bet;

    public function setUp()
    {
        $this->bettingManager = new BettingManger();
        $this->bet = new Bet();
        $this->bet->setType('Single Bet')
            ->setValue(20)
            ->setOption(1);
        parent::setUp();
    }

    public function testGetBettingResultForBettingType()
    {
        $this->assertEquals([1 => 60], $this->bettingManager->getBettingResultForBettingType(
            [$this->bet],
            1
        ));
    }


    public function testGetBettingResultForBettingTypeNeg()
    {

        $this->assertEquals([1 => 00], $this->bettingManager->getBettingResultForBettingType(
            [$this->bet],
            10
        ));
    }

    public function testGetBettingResultForBettingTypeException()
    {
        $this->expectException(BettingTypeAdaptorNotFoundException::class);
        $this->bet->setType('foo');
        $this->bettingManager->getBettingResultForBettingType(
            [$this->bet],
            10
        );
    }
}