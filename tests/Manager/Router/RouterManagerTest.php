<?php

namespace Skybet\Tests\Managers\Router;


use PHPUnit_Framework_TestCase;
use Skybet\Managers\Router\RouterManager;

class RouterManagerTest extends PHPUnit_Framework_TestCase
{
    public function testLoadAndGetRoutes()
    {
        $router = new RouterManager();
        $this->assertEquals(4, $router->getAllRoutes()->count());
    }
}